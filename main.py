from argparse import ArgumentParser
from model_files.simulation import run_simulation


def handle_cmdline():
    parser = ArgumentParser()

    parser.add_argument(
        '-c', '--config',
        default='02-configurations/baseconfig.yaml',
        type=str,
        help='Configuration file defining all input files and parameters used.')

    return parser.parse_args()


def main():
    parsed_args = handle_cmdline()
    run_simulation(parsed_args)


if __name__ == '__main__':
    main()

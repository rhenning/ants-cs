import os
import shutil
import pandas as pd
from datetime import datetime
from pathlib import Path


reporoot_dir = Path(__file__).resolve().parent

DA_price_path = reporoot_dir / 'auction_spot_prices_netherlands_2021.csv'

ID_data_dir = reporoot_dir / 'ID_prices'

generate_ID_price_paths = (file.path for file in os.scandir(ID_data_dir))

target_path = reporoot_dir / '01-datasets' / 'prices' / '2021_APX_DA_ID.csv'

# 1. : create pandas dataframe from DA prices
DA_prices = pd.read_csv(DA_price_path, sep=';', decimal='.', header=0, index_col=False)
hour_names = ['Hour {}'.format(i) for i in range(1,25)]
hour_names[2] = 'Hour 3A'

dt_index = (datetime.strptime(row['Delivery day'], '%d-%m-%Y') + pd.to_timedelta(hour, unit='h')
            for i, row in DA_prices.iterrows() for hour in range(24))

DA_price_values = (row[hour_string] for i, row in DA_prices.iterrows() for hour_string in hour_names)

ID3_price_values = []
for ID_price_path in generate_ID_price_paths:
    ID_prices = pd.read_csv(ID_price_path, sep=',', decimal='.', header=0, index_col=False)
    ID3_prices_hourly = ID_prices.loc[ID_prices['IndexName'] == 'ID3'].loc[ID_prices['TimeResolution'] == 'H']
    ID3_prices_hourly_list = ID3_prices_hourly['IndexPrice'].tolist()
    if len(ID3_prices_hourly_list) == 23:
        ID3_prices_hourly_list.insert(2, 34.1)
    if len(ID3_prices_hourly_list) == 25:
        del ID3_prices_hourly_list[2]
    ID3_price_values.extend(ID3_prices_hourly_list)

data = {'Time': dt_index, 'Day-Ahead Price': DA_price_values, 'ID Price': ID3_price_values}

output_df = pd.DataFrame(data=data).fillna(value=37)

with open(target_path, newline='', mode='w+') as file:
    output_df.to_csv(file, sep=';', index=False)

print(output_df[-30:])
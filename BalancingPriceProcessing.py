import pandas as pd
import numpy as np
from pathlib import Path
import xml.etree.ElementTree as ET

reporoot_dir = Path(__file__).resolve().parent

balancing_price_path = reporoot_dir / '01-datasets' / 'prices' / 'NL_IMBALANCE_PRICES_2021.xml'

prices_file_path = reporoot_dir / '01-datasets' / 'prices' / '2021_APX_DA_ID.csv'

prices_df = pd.read_csv(prices_file_path, sep=';')
prices_df["positive imbalance price"] = np.NaN
prices_df["negative imbalance price"] = np.NaN

tree = ET.parse(balancing_price_path)

root = tree.getroot()

namespace = {'default': 'urn:iec62325.351:tc57wg16:451-6:balancingdocument:4:4'}

positive_imbalance = False

for j, ts in enumerate(root.findall('default:TimeSeries', namespace)):
    # there are two time series per day, one for positive and one for negative
    # imbalance prices
    day = j//2
    # Time series alternate between positive and negative imbalance price
    positive_imbalance = not positive_imbalance
    period = ts.find('default:Period', namespace)
    time_interval = period.find('default:timeInterval', namespace)

    # need to add 4 extra entries to default of 96 quarters per day because of DST extra hour
    imb_prices = np.zeros(100)
    for i, point in enumerate(period.findall('default:Point', namespace)):
        imb_prices[i] = point.find('default:imbalance_Price.amount', namespace).text
    # average over every 4 consecutive elements of quaterly balancing prices to get hourly average:
    imb_prices_hourly_avg = np.mean(imb_prices.reshape(-1, 4), axis=1)

    price_type = 'positive imbalance price' if positive_imbalance else 'negative imbalance price'

    prices_df[price_type].iloc[day * 24:(day + 1) * 24] = imb_prices_hourly_avg[:24]
    # print(prices_df.head())
    # print(imb_prices[:96])
    # print(imb_prices_hourly_avg[:24])
    # break

with open(prices_file_path, newline='', mode='w+') as file:
    prices_df.to_csv(file, sep=';', index=False)
from datetime import datetime, timedelta
from datetime import time as dt_time
from functools import reduce
import numpy as np
import pyomo.environ as pyo
import pandas as pd
from math import floor, ceil, pow

### TO-DO: it would be much more elegant to create an abstract model here, create a data sheet in simulation.py,
# and pass the data sheet here to fill the abstract model.

### helper functions for time checking in model
def _is_time_between(begin_time, end_time, check_time):
    # helper function to check whether given time is between two other times
    # (e.g.: between arrival and departure time of EV)
    if begin_time < end_time:
        # first case: both times are on the same day
        return begin_time <= check_time <= end_time
    else:
        # second case: end_time is one day later (typically in the morning)
        return check_time >= begin_time or check_time <= end_time


def _round_time_down(given_time, time_step):
    """
    helper function to round given time down to nearest time step
    :param given_time:
    :param time_step:
    :return: rounded time
    """
    t_hour = given_time.hour
    minute = given_time.minute
    rounded_time_minute = time_step * int(floor(minute / time_step))
    return dt_time(hour=t_hour, minute=rounded_time_minute)


def generate_ev_constraints(model, state='day-ahead'):
    """
    :param state: day-ahead or intraday optimization
    :param model: initialized pyomo model with variables and index set
    :return: pyo.ConstraintList for all EV specific constraints
    """
    # general constraints
    # EV charge is bounded by max charge rate for each EV at any time
    def max_charge_rule(model, t, s, c, ev_i):
        # if no congestion, charging is only constraint by max power of charger
        return model.flex_power[t, s, c, ev_i] <= model.evs[ev_i].max_charge_power

    # EV charge may also be bounded by individual capacity limitation in case of congestion:
    def ind_cap_limit_rule(model, t, s, c, ev_i):
        if state == 'day-ahead':
            # day-ahead we assume that the inflexible load will be the total forecasted inflexible load
            # divide by the number of customers, with an added safety factor >1
            inflex_load_assumption = model.scenario_load[t, s]/model.n_cus * model.inflex_load_safety_fac
            if 'variable' in model.cm_method:
                inflex_load_assumption = 0
            if 'Interruptible' in model.cm_method:
                # if c == 0:
                #     # no congestion at all, no additional limitation present
                #     return model.flex_power[t, s, c, ev_i] <= model.evs[ev_i].max_charge_power
                if c == 100:
                    # complete congestion, everyone will be curtailed
                    return model.flex_power[t, s, c, ev_i] <= model.max_cap_in_curt
                else:
                    # assert 0 < c < 100
                    # c percent of EVs are curtailed
                    last_ev_index = ceil(c / 100 * model.n_evs)
                    # use a pre-computed random ordering of EVs for every time step and scenario
                    if ev_i in model.curtailment_order[t, s][:last_ev_index]:
                        return model.flex_power[t, s, c, ev_i] <= model.max_cap_in_curt
                    # else:
                    #     return model.flex_power[t, s, c, ev_i] <= model.evs[ev_i].max_charge_power
        elif state == 'intraday':
            # in intraday balancing stage we assume that the inflexible load of the customer is known for the next 24h
            # inflex_load_assumption = model.evs[ev_i].customer.inflex_power.loc[model.idx_to_timestamp_dict[t]]
            if t < 24:
                inflex_load_assumption = model.evs[ev_i].customer.inflex_power[model.idx_to_timestamp_dict[t]]
                    # model.scenario_load[t, s] / model.n_cus
            else:
                inflex_load_assumption = model.scenario_load[t, s] / model.n_cus * model.inflex_load_safety_fac
            if 'Interruptible' in model.cm_method:
                if t == model.lowest_price_time_step:
                    if ev_i in model.curtailment_order[t, 0][:model.curtailed_at_lowest_price]:
                        return model.flex_power[t, s, c, ev_i] <= model.max_cap_in_curt
                elif t == model.second_lowest_price_time_step:
                    if ev_i in model.curtailment_order[t, 0][:model.curtailed_at_nextl_price]:
                        return model.flex_power[t, s, c, ev_i] <= model.max_cap_in_curt
            if 'variable' in model.cm_method:
                inflex_load_assumption = 0
                # else:
                #     return model.flex_power[t, s, c, ev_i] <= model.evs[ev_i].max_charge_power
        # in non-Interruptible methods, there is no curtailment, the capacity limitations are known then
        return model.flex_power[t, s, c, ev_i] <= max(model.base_sub[ev_i] +
                                                       model.var_sub[ev_i] * model.act_factor[t] -
                                                        inflex_load_assumption,
                                                        0)


    # in pooled subscriptions, only the sum over all EVs is bounded by the summed subscription level:
    # def pooled_cap_limit_rule(model, t, s):
    #     return sum(model.flex_power[t, s, c, ev_i] for ev_i in model.ev_ind)\
    #            <= model.n_evs*(model.def_cap_sub * model.cl_factor[t] - model.avg_inflex_load[t]/model.n_cus)


    # Vehicle-to-Grid is also bounded by max charge rate for each EV at any time
    def v2g_rule(model, t, s, c, ev_i):
        if model.v2g:
            return (- model.flex_power[t, s, c, ev_i]) <= model.evs[ev_i].max_charge_power
        else:
            return model.flex_power[t, s, c, ev_i] >= 0

    # state of charge is bounded by battery size for each EV at any time.
    # lower bound of 0 is already enforced in creation of state-of-charge variable (NonNegativeReals)
    def battery_size_rule(model, t, s, c, ev_i):
        return model.stateOfCharge[t, s, c, ev_i] <= model.evs[ev_i].battery_size

    # relate state of charge to charging and depletion of daily demand
    def charge_update_rule(model, t, s, c, ev_i):
        ev = model.evs[ev_i]
        # set initial charges to current state of charge of the battery
        if t == 0:
            # print("starting charge: " + str(ev.current_charge))
            return model.stateOfCharge[t, s, c, ev_i] == ev.current_charge
        # if time is not zero, we can take t-1 as the previous time step
        # reduce state of charge by daily demand every day on arrival
        if datetime.time(model.idx_to_timestamp_dict[t]) == _round_time_down(ev.arrival_time, model.time_step):
            # print("resetting state of charge: " + str(datetime.time(idx_to_timestamp_dict[time])))
            return model.stateOfCharge[t, s, c, ev_i] == model.stateOfCharge[t - 1, s, c, ev_i] \
                   - ev.daily_demand
        # dynamically update state of charge afterwards, based on SOC in previous time step
        return model.stateOfCharge[t, s, c, ev_i] == model.stateOfCharge[t - 1, s, c, ev_i] \
               + ev.charging_efficiency \
               * model.flex_power[t - 1, s, c, ev_i] * model.time_step / 60

    # # charging is only possible between arrival and departure time
    def home_charge_rule(model, t, s, c, ev_i):
        ev = model.evs[ev_i]
        arrival_time = datetime.time(ev.arrival_time)
        departure_time = datetime.time(ev.departure_time)
        current_time = datetime.time(model.idx_to_timestamp_dict[t])
        if not _is_time_between(arrival_time, departure_time, current_time):
            return model.flex_power[t, s, c, ev_i] == 0
        return pyo.Constraint.Skip

    # charge requirement for each EV each morning at departure time
    # target charge can be left unsatisfied at cost of VoEVUL (Value of EV unsatisfied load) given in inputs
    # (NOTE: the model only optimizes for the last departure time. Thus, a slightly longer time horizon
    # should be given to avoid boundary effects of the optimization.)
    def charge_at_departure_rule(model, t, s, c, ev_i):
        # if time is departure time, set constraint at target state of charge percentage
        if datetime.time(model.idx_to_timestamp_dict[t]) == _round_time_down(
                model.evs[ev_i].departure_time, model.time_step):
            # print("resetting charge requirement at time: " + str(datetime.time(idx_to_timestamp_dict[time])))
            return model.stateOfCharge[t, s, c, ev_i] >= model.evs[ev_i].target_charge - model.EVUL[s, c, ev_i]
        # if time is not departure time: no action needed
        return pyo.Constraint.Skip

    model.maxChargeConstraint = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind,
                                               model.ev_ind, rule=max_charge_rule)
    # if model.pooled:
    #     model.CapLimitConstraint = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind, rule=pooled_cap_limit_rule)
    # else:
    model.CapLimitConstraint = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind,
                                              model.ev_ind, rule=ind_cap_limit_rule)
    model.v2gConstraint = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind,
                                         model.ev_ind, rule=v2g_rule)
    model.BatterySizeConstraint = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind,
                                                 model.ev_ind, rule=battery_size_rule)
    model.ChargeUpdateConstraint = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind,
                                                  model.ev_ind, rule=charge_update_rule)
    model.HomeChargeConstraint = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind,
                                                model.ev_ind, rule=home_charge_rule)
    model.ChargeDepartureConstraint = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind,
                                                     model.ev_ind, rule=charge_at_departure_rule)
    return


def generate_bigm_constraints(model):
    # introduce binary variable to determine wether net load is positive or negative:
    model.bin = pyo.Var(model.time_ind, domain=pyo.Binary)
    p_max = 200
    p_min = -200
    c = 0

    # binary constraints:
    model.upper_bound = pyo.Constraint(model.time_ind,
                                               rule=lambda model, t: model.net_power[t, 0, c] <= p_max * model.bin[t])
    model.lower_bound = pyo.Constraint(model.time_ind,
                                               rule=lambda model, t: model.net_power[t, 0, c] >= p_min * (1-model.bin[t]))

    # auxiliary constraints:
    model.upper_aux = pyo.Constraint(model.time_ind,
                                               rule=lambda model, t: model.aux[t] <= p_max * model.bin[t])
    model.lower_aux = pyo.Constraint(model.time_ind,
                                               rule=lambda model, t: model.aux[t] >= p_min * model.bin[t])
    model.aux_le_net = pyo.Constraint(model.time_ind,
                                               rule=lambda model, t: model.aux[t] <= model.net_power[t, 0, c]-p_min*(1-model.bin[t])
                                              )
    model.aux_ge_net = pyo.Constraint(model.time_ind,
                                               rule=lambda model, t: model.aux[t] >= model.net_power[t, 0, c]-p_max*(1-model.bin[t])
                                              )


def create_common_variables(model):
    ### Variable transformations:
    # introduce total load
    model.total_load = pyo.Var(model.time_ind, model.scen_ind, model.curt_ind, domain=pyo.Reals)
    # decision variable flex power, indexed by time, scenario and EV
    model.flex_power = pyo.Var(model.time_ind, model.scen_ind, model.curt_ind, model.ev_ind, domain=pyo.Reals)
    # introduce total flexible load
    model.total_flex_load = pyo.Var(model.time_ind, model.scen_ind, model.curt_ind, domain=pyo.Reals)
    # introduce net power per time index
    model.net_power = pyo.Var(model.time_ind, model.scen_ind, model.curt_ind, domain=pyo.Reals)

    # declare a state variable, state of charge of battery.
    # by making this non-negative we already include the lower bound of 0 for state of charge.
    model.stateOfCharge = pyo.Var(model.time_ind, model.scen_ind, model.curt_ind, model.ev_ind,
                                  domain=pyo.NonNegativeReals)

    # introduce EV Unsatisfied Load: EV can be charged to less than desired charge for a high penalty.
    # this is not dependent on any individual time index, rather it is based on a scenario of overnight charging
    # since only the total energy in the battery at the end matters and not at which time it was charged
    model.EVUL = pyo.Var(model.scen_ind, model.curt_ind, model.ev_ind, domain=pyo.NonNegativeReals)

    # total flexible load is the sum of all users' flexible loads
    def total_load_rule(model, t, s, c):
        return model.total_load[t, s, c] == sum(model.flex_power[t, s, c, ev_i] for ev_i in model.ev_ind) \
                                             + model.scenario_load[t, s]


    # net power is the difference between purchased and used inflexible + flexible loads at time t for scenario s
    def net_power_rule(model, t, s, c):
        return model.net_power[t, s, c] == model.total_load[t, s, c] - model.purchased_power[t]

    # Absolute value transformation: introduce positive and negative value of net power:
    model.net_power_pos = pyo.Var(model.time_ind, model.scen_ind, model.curt_ind, domain=pyo.NonNegativeReals)
    model.net_power_neg = pyo.Var(model.time_ind, model.scen_ind, model.curt_ind, domain=pyo.NonNegativeReals)

    def net_power_transform_rule(model, t, s, c):
        return model.net_power[t, s, c] == model.net_power_pos[t, s, c] - model.net_power_neg[t, s, c]

    model.total_load_relation = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind, rule=total_load_rule)
    model.net_power_relation = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind, rule=net_power_rule)
    model.net_power_transform = pyo.Constraint(model.time_ind, model.scen_ind, model.curt_ind, rule=net_power_transform_rule)


# scheduling functions
# central
def day_ahead_optimization(DA_model):
    """

    :param model:
    :param price_offset:
    :return:
    """
    print('Solving Day-ahead optimization problem ...')

    # scenarios for day-ahead problem start at 1 (second scenario), as 0 (first scenario) is reserved for intraday realization
    scenario_indexes = range(1, DA_model.n_scenarios)

    # day-ahead specific decision variable: purchased power (in ID model this is a fixed PARAMETER instead)
    DA_model.purchased_power = pyo.Var(DA_model.time_ind, domain=pyo.NonNegativeReals)

    create_common_variables(DA_model)

    generate_ev_constraints(DA_model, state='day-ahead')

    def obj_expression(DA_model):
        return DA_model.time_step/60 * 1/DA_model.n_scenarios * \
               sum(                                                       # sum over scenarios
                   sum(                                                   # sum over different degrees of curtailment
                       (
                        sum(                                                # sum over time steps
                            DA_model.curt_prob[t, c] *
                            (DA_model.DA_prices[t] * DA_model.total_load[t, s, c] +
                            DA_model.imbalance_offset *
                             (DA_model.net_power_pos[t, s, c] + DA_model.net_power_neg[t, s, c])
                             )
                            for t in DA_model.time_ind
                        )
                        + DA_model.VoEVUL * sum(                                        # sum over EVs
                             DA_model.EVUL[s, c, ev_i] for ev_i in DA_model.ev_ind
                        )
                       )
                        for c in DA_model.curt_ind
                   )
                    for s in scenario_indexes
               )


    DA_model.OBJ = pyo.Objective(rule=obj_expression, sense=pyo.minimize)

    # solve model
    opt = pyo.SolverFactory('gurobi')
    results = opt.solve(DA_model)
    return results

def intra_day_optimization(ID_model):
    print('Solving intra-day balancing optimization ...')

    # ID model uses the first (index 0) load scenario as the actually realized values:
    s = 0
    c = 0
    create_common_variables(ID_model)

    generate_ev_constraints(ID_model, state='intraday')

    # bigM constraints in case the objective function is asymmetric in net power
    # introduce auxiliary variable to transform non-linear problem into mixed integer linear program:
    # if ID_model.buysell_offset:
    #     ID_model.aux = pyo.Var(ID_model.time_ind, domain=pyo.NonNegativeReals)
    #     generate_bigm_constraints(ID_model)

    def obj_expression(ID_model):
        return (
                sum(
                ID_model.DA_prices[t] * ID_model.net_power[t, s, c] +
                ID_model.imbalance_offset * (ID_model.net_power_pos[t, s, c] + ID_model.net_power_neg[t, s, c])
                for t in ID_model.time_ind
                )
                + ID_model.VoEVUL * sum(ID_model.EVUL[s, c, ev_i] for ev_i in ID_model.ev_ind)
        )

        #
        # return sum(ID_model.ID_prices[t] * ID_model.time_step/60 * ID_model.net_power[t, s]
        #            + ID_model.buysell_offset * ID_model.time_step/60 * ID_model.aux[t]
        #            for t in ID_model.time_ind)
    
    ID_model.OBJ = pyo.Objective(rule=obj_expression, sense=pyo.minimize)

    # solve model
    opt = pyo.SolverFactory('gurobi')
    opt.solve(ID_model)
    
    return

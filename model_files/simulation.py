from pathlib import Path
from os import listdir
from yaml import load, FullLoader
from math import ceil
import numpy as np
import pandas as pd
import pyomo.environ as pyo
import pickle
from datetime import datetime, timedelta

import matplotlib.pyplot as plt

from functools import reduce

import model_files.plots as plots
from model_files.customer_classes import Customer, EV, HVAC
from model_files.optimization import day_ahead_optimization, intra_day_optimization, _is_time_between

# establish file path of root directory of the repository:
reporoot_dir = Path(__file__).resolve().parent.parent

### NOTE: conversion of load from kWh to kW average per time interval is not done yet.
# to be decided whether this will be necessary again.
def initialize_customers(customer_load_dir, n_customers, time_step):
    print('Loading user data ...')
    # construct absolute path of customer load directory
    load_dir_path = reporoot_dir / Path(customer_load_dir)
    # create list of files for each customer load file
    file_list = listdir(load_dir_path)
    # initialize counter for customers
    counter = 1
    # initialize dict for customers per neighborhood:
    customers_dict = {}
    # loop over neighborhoods - in the end only one neighborhood was implemented, multiple neighborhoods
    # could be investigated in the future
    for neighborhood_key in ['A']:
        # initialize the neighborhood to contain a dictionary which will be filled with customers:
        this_neighborhood_customers = {}
        # we create one customer object for each customer:
        for i in range(n_customers):
            # create customers id from neighborhood and 3-digit number.
            # the 3 digit number starts from 0 again for each neighborhood.
            customer_id = neighborhood_key + f'{i:03}'
            # create file path for data file from load dir and file name from list:
            filepath = load_dir_path / file_list[counter]
            # read file with pandas, automatically parsing the dates and converting to a Series (squeeze)
            load_df_kWh = pd.read_csv(filepath, sep=';', decimal='.', header=0, index_col=0,
                                           parse_dates=True).squeeze("columns")
            # resample to given time step intervals, summing over entries
            if time_step >= 15:
                load_df_kWh = load_df_kWh.resample('{}T'.format(time_step)).sum()
            # add a year to input data to get same year as prices
            load_df_kWh.index = load_df_kWh.index + pd.offsets.DateOffset(years=1)
            # convert time series from kWh to kW
            load_df_kW = load_df_kWh.multiply(int(60 / time_step))
            # create new customer object and add it to the neighborhood:
            this_neighborhood_customers[customer_id] = Customer(customer_id, neighborhood_key, load_df_kW)
            # increment counter
            counter += 1
        # change neighborhoods dictionary to contain the dictionary of customers for each neighborhood:
        customers_dict = this_neighborhood_customers
    return customers_dict


def draw_indices(population_size, sample_size):
    neighborhood_ev_ids = {}
    # in the end only one neighborhood was implemented - might look at multiple neighborhoods in the future
    neighborhood_key = 'A'
    n_customers = population_size
    # draw given number of evs out of given number of customers without replacement (per neighborhood)
    this_neighborhood_ev_indices = np.random.choice(population_size, sample_size, replace=False)
    # transform numbers into IDs
    this_neighborhood_ev_ids = list(range(len(this_neighborhood_ev_indices)))
    for i, index in enumerate(this_neighborhood_ev_indices):
        customer_with_ev_id = neighborhood_key + f'{index:03}'
        this_neighborhood_ev_ids[i] = customer_with_ev_id

    return this_neighborhood_ev_ids


def load_evs(neighborhood_customers, ev_data_path, customer_with_ev_ids):
    print('Loading EV constraints ...')
    # look up absolute path of ev
    load_dir_path = reporoot_dir / Path(ev_data_path)
    # load EV constraints to pandas dataframe
    ev_constraint_df = pd.read_csv(load_dir_path, sep=';')
    # compute number of distinct EV profiles
    n_ev_profiles = len(ev_constraint_df.index)
    # compute number of total EVs selected
    n_ev = max(len(customer_with_ev_ids), n_ev_profiles)
    # randomly select n_ev EVs from EV profiles
    ev_indices = np.random.choice(n_ev_profiles, n_ev, replace=False)
    ev_counter = 0
    neighborhood_key = 'A'
    for customer_id in customer_with_ev_ids:
        # once ev counter surpasses the number of ev profiles, we start from the beginning again
        this_ev_index = ev_indices[ev_counter % n_ev_profiles]
        # select row from ev constraints data frame
        row = ev_constraint_df.iloc[this_ev_index, :]
        this_customer = neighborhood_customers[customer_id]
        this_ev = EV(ev_counter+1, this_customer, neighborhood_key, pd.to_datetime(row['Arrival time']),
                     pd.to_datetime(row['Departure time']),
                     float(row['Starting state of charge percentage']), float(row['Target state of charge']),
                     row['Battery size [kWh]'], row['max charge rate [kW]'],
                     float(row['Charging efficiency rate']), float(row['Daily demand mean [kWh]']))
        # initialize the ev of this customer with the given constraints
        setattr(this_customer, 'ev', this_ev)
        # set variable subscription of customer to value given from csv
        setattr(this_customer, 'var_sub', row['variable subscription [kW]'])
        # increment ev counter
        ev_counter += 1


def load_hvacs(customer_dict, hvac_data_path, households_with_hvac_ids=None):
    pass


def load_prices(price_data_path, time_step, start, end):
    price_df_MWh = pd.read_csv(price_data_path, sep=';', decimal='.', header=0, index_col=0,
                                           parse_dates=True).squeeze("columns")
    # resample to given time step intervals, padding the result at the intermediate time steps
    price_df_MWh = price_df_MWh.resample('{}T'.format(time_step)).ffill()
    # convert to prices per kWh
    price_df_kWh = price_df_MWh.mul(0.001)
    DA_price_ts = pd.Series(price_df_kWh['Day-Ahead Price'].values, index=price_df_kWh.index)[start:end]
    ID_price_ts = pd.Series(price_df_kWh['ID Price'].values, index=price_df_kWh.index)[start:end]
    PosBalancing_price_ts = pd.Series(price_df_kWh['positive imbalance price'].values,
                                      index=price_df_kWh.index)[start:end]
    return DA_price_ts, ID_price_ts, PosBalancing_price_ts


def assign_base_sub(cus: Customer, penalty: float, subscription_kW_price: float):
    """
    computes the best value of firm capacity for this consumer
    TO-DO::: add correct time step. Currently only works for 1h time step where power in kW = consumption in kWh
    :param cus:
    :param penalty: penalty charge in Euro for kWh above level
    :return:
    """
    level_step = 0.5
    min_level = 1
    max_level = 5
    levels = [i/10.0 for i in range(min_level*10, max_level*10 + 1, int(level_step*10))]
    # print(levels)
    load_above_level = {l: 0 for l in levels}
    for t, power in cus.inflex_power.items():
        for l in levels:
            if power > l:
                load_above_level[l] += power - l
            else:
                break
    total_price = {}
    current_choice = 1.0
    for l in levels:
        total_price[l] = l*subscription_kW_price + load_above_level[l]*penalty
        if l > 1.1:
            prev_level = round(l-level_step, 1)
            # once prices start to increase we can take the previous level, they will only keep increasing from here
            if total_price[l] > total_price[prev_level]:
                current_choice = prev_level
                break
            elif int(l) == max_level:
                current_choice = max_level
    cus.base_sub = current_choice
    return current_choice


def load_capacity_factors(cf_path, time_step):
    cf_df = pd.read_csv(cf_path, sep=';', decimal='.', header=0, index_col=0,
                                           parse_dates=True)
    # resample to given time step intervals, padding the result at the intermediate time steps
    cf_df = cf_df.resample('{}T'.format(time_step)).ffill()
    return cf_df


def generate_scenario(time_series, std_dev, decay_param):
    """
    Function to generate different scenarios for load and capacity factors,
    based on given historical data and expected standard deviations of their distributions.
    :param time_series: historical load time series
    :param std_dev: standard deviation for load (in percent of value, as decimal number)
    :param decay_param: decay of previously accumulated error
    :return: one scenario with random draws based on given data
    """
    # initialize decimal errors for this data series at same length as given load time series
    decimal_errors = np.zeros_like(time_series.index, dtype=float)
    current_error = np.random.normal(0, std_dev)
    for t in range(1, len(time_series.index)):
        decimal_errors[t] = current_error
        current_error = np.random.normal(decay_param*current_error, std_dev)
    # print(percentage_errors)
    # multiply original time series with relative error (= decimal error + 1)
    scenario_ts = time_series.mul(pd.Series(data=decimal_errors, index=time_series.index).add(1))
    return scenario_ts


def swap_times(scenario_data_ts):
    """
    function to randomly swap some time series entries to create "x" variable randomness in addition to "y" randomness
    swapping may make more sense to loads
    :param scenario_data_ts:
    :return:
    """
    swapped = np.zeros_like(scenario_data_ts.index, dtype=bool)
    # initialize new array to return later
    swapped_data = np.zeros_like(scenario_data_ts.index, dtype=float)
    for i in range(len(scenario_data_ts.index)-2):
        # only swap if this entry has not already been swapped
        if not swapped[i]:
            # swap by either 0, 1 or 2 index positions:
            swap_index = np.random.randint(0, 3)
            # perform exchange of entries in new data series
            swapped_data[i] = scenario_data_ts.iloc[i+swap_index]
            swapped_data[i+swap_index] = scenario_data_ts.iloc[i]
            # mark exchanged entry as swapped
            swapped[i+swap_index] = True
    return pd.Series(swapped_data, index=scenario_data_ts.index)

def activate_cl(model, cong_time_idx, scen_idx, activation_level=None,
                neighboring_times=None, emergency_cl_factor=0.5, activation_type=""):
    if neighboring_times is None:
        neighboring_times = model.neighboring_times
    cong_start_time = cong_time_idx - neighboring_times
    cong_end_time = cong_time_idx + neighboring_times
    if cong_time_idx < neighboring_times:
        # if cong happens here, we can not use the neighboring time since it would be on the previous day
        cong_start_time = 0
    elif cong_time_idx > len(model.time_ind) - neighboring_times:
        cong_end_time =len(model.time_ind)
    for t in range(cong_start_time, cong_end_time+1):
        # only overwrite the existing cl_factor if it is smaller than the currently set value
        current_cl_factor = pyo.value(model.act_factor[t, scen_idx])
        if current_cl_factor > activation_level:
            model.act_factor[t, scen_idx] = activation_level


def load_configs(args):
    # read config file
    config_file_path = Path(args.config).resolve()
    with open(config_file_path) as f:
        configs = load(f, Loader=FullLoader)
    return configs

def load_data(configs, n_evs, strategy):
    data = {}

    data['time step'] = configs['time step']

    ### initialize neighborhoods with customers and evs
    data['customers'] = initialize_customers(configs['load data folder'], configs['n customers'],
                                                      data['time step'])
    # data['customers dict'] = flatten_customers_dict(data['customers'])

    # add EVs to households
    ev_indices = draw_indices(configs['n customers'], n_evs)
    print(ev_indices)
    load_evs(data['customers'], configs['EV parameters path'], ev_indices)

    #create dict of flex users only
    data['flex users dict'] = {cus_id: customer for cus_id, customer in data['customers'].items() if
                               customer.ev is not None}

    # determine subscription levels and total sold base capacty
    total_base_cap = 0
    penalty = configs['high charge kWh'] - configs['low charge kWh']
    price_per_kW = configs['base sub kW cost']
    for cus in data['customers'].values():
        # assign base capacity based on customer load
        assign_base_sub(cus, penalty, price_per_kW)
        if cus.ev is not None:
            if 'static' in strategy:
                # no variable capacity, adjust base capacity based on EV demand
                # +.5 subscribed capacity per 3 kWh of demand
                added_cap_for_ev = 0.5*cus.ev.daily_demand//2.5
                cus.base_sub += added_cap_for_ev
                # variable capacity is 0 in this approach
                cus.var_sub = 0
            elif 'Interruptible' in strategy:
                # capacity is not initially limited in Interruptible proposal, only curtailed in real time to 5 kW
                # in order to implement this curtailment we use a large variable capacity subscription (bigger
                # than the EV charging capacity) that can be curtailed down to at most 5 kW in real time,
                # corresponding to an activation factor of 0.25
                cus.base_sub = 0
                cus.var_sub = 20
            elif 'variable' in strategy:
                # reduce base capacity based on variable capacity choice: reduce by one third of variable capacity
                # down to minimum of 1
                # cus.base_sub = max(1, cus.base_sub - cus.var_sub//3)
                # alternative: assign 2 kW base sub to every customer
                cus.base_sub = 0
        total_base_cap += cus.base_sub
        # if cus.ident in ['A009']:
        #     plots.static_cap_sub_visual(cus)
    print("Total assigned base capacity: " + str(total_base_cap))

    data['flex base sum'] = sum((cus.base_sub for cus in data['flex users dict'].values()))
    print('Base capacity sum of flexible users: ' + str(data['flex base sum']))

    data['flex var sum'] = sum((cus.var_sub for cus in data['flex users dict'].values()))
    print('Variable capacity sum of flexible users: ' + str(data['flex var sum']))
    # for cus in data['flex users dict'].values():
    #     print('Flex user id: ' + str(cus.ident))
    #     print('Var sub: ' + str(cus.var_sub))
    #     print('Base sub: ' + str(cus.base_sub))

    data['evs dict'] = {cus.ev.ev_id: cus.ev for cus in data['customers'].values() if cus.ev is not None}

    datetime_format = configs['datetime format']
    start_date = datetime.strptime(configs['start datetime'], datetime_format)
    end_date = datetime.strptime(configs['end datetime'], datetime_format)

    # initialize prices time series
    data['DA prices'], data['ID prices'], data['+bal prices'] = \
        load_prices(configs['price path'], data['time step'], start_date, end_date)

    return data

def initialize_model(configs, data):
    # initialize pyomo model, to which all data is attached
    model = pyo.ConcreteModel()

    customers = data['customers']

    # maximum allowable curtailment during congestion events
    model.max_cap_in_curt = configs['max cap in curt']
    
    model.cus_ind = pyo.Set(initialize=[key for key in customers.keys()])
    model.n_cus = len(customers.keys())
    model.customers = pyo.Param(model.cus_ind, initialize=customers, within=pyo.Any)

    model.trafo_size = configs['trafo size']

    model.v2g = configs['v2g']

    # collect flex devices
    model.evs = {customer.ident: customer.ev for customer in customers.values()
                 if customer.ev is not None}
    model.hvacs = {customer.ident: customer.hvac for customer in customers.values()
                 if customer.hvac is not None}

    ### market model parameters
    # assumption for cost of imbalances in DA model (additional to DA price at same time)
    model.imbalance_offset = configs['imbalance price offset']
    # assumption for additional cost of buying relative to selling on ID model
    model.buysell_offset = configs['buy-sell offset']
    # assumption for value of unsatisfied EV load at the end of the night:
    model.VoEVUL = configs['VoEVUL']

    model.n_scenarios = configs['n_scenarios']
    model.ev_ind = pyo.Set(initialize=[key for key in model.evs.keys()])
    model.n_evs = len(model.evs.keys())
    model.hvac_ind = pyo.Set(initialize=[key for key in model.hvacs.keys()])
    model.time_step = data['time step']

    ### capacity limitation and congestion parameters
    # base subscription levels associated with EVs
    model.base_sub = pyo.Param(model.ev_ind,
                               initialize={ev_i: ev.customer.base_sub for ev_i, ev in model.evs.items()},
                               within=pyo.Any)
    # variable subscription levels associated with EVs
    model.var_sub = pyo.Param(model.ev_ind,
                               initialize={ev_i: ev.customer.var_sub for ev_i, ev in model.evs.items()},
                              within=pyo.Any)
    
    # default capacity subscription value for users with EV
    model.def_cap_sub = configs['def cap sub']
    # pooled (true) or individual (false) capacity limitations:
    model.pooled = configs['pooled']
    # proportional or on/off activation of capacity limitation
    model.prop_act = configs['proportional act']
    # number of neighboring time steps for which cap limit is also activated:
    model.neighboring_times = configs['neighboring times']
    # safety factor applied to inflexible load estimates of users
    model.inflex_load_safety_fac = configs['inflex load safety factor']

    return model

def add_model_time_data(model, configs, load_scenarios, n_evs, current_time,
                        opt_start, opt_end, data, cm_method):
    ### time index housekeeping
    opt_times = pd.date_range(opt_start, opt_end, freq='{}min'.format(configs['time step']))
    # helper dict for converting between time index and times:
    model.idx_to_timestamp_dict = dict(enumerate(opt_times))
    model.timestamp_to_idx_dict = {v: k for k, v in model.idx_to_timestamp_dict.items()}
    model.time_ind = pyo.Set(initialize=[int(key) for key in model.idx_to_timestamp_dict.keys()])

    model.n_scenarios = len(load_scenarios)

    # NOTE: day-ahead scenarios are 1,..,n_scenario, ID scenario is 0
    model.scen_ind = pyo.Set(initialize=range(model.n_scenarios))

    def load_init(model, t, s):
        return load_scenarios[s].loc[model.idx_to_timestamp_dict[t]]

    model.scenario_load = pyo.Param(model.time_ind, model.scen_ind, initialize=load_init)

    def avg_inflex_load_init(model, t):
        return sum(model.scenario_load[t, s] for s in model.scen_ind) / len(load_scenarios)

    model.avg_inflex_load = pyo.Param(model.time_ind, initialize=avg_inflex_load_init)

    def DA_price_init(model, t):
        return data['DA prices'].loc[model.idx_to_timestamp_dict[t]]
    model.DA_prices = pyo.Param(model.time_ind, initialize=DA_price_init)

    def ID_price_init(model, t):
        return data['ID prices'].loc[model.idx_to_timestamp_dict[t]]
    model.ID_prices = pyo.Param(model.time_ind, initialize=ID_price_init)

    def Bal_price_init(model, t):
        return data['+bal prices'].loc[model.idx_to_timestamp_dict[t]]
    model.pBal_prices = pyo.Param(model.time_ind, initialize=Bal_price_init)

    model.cm_method = cm_method
    model.curt_ind = pyo.Set(initialize=[key for key in
                                         configs['Interruptible curtailment probabilities']['optimistic'].keys()])

    # find lowest price time steps in first 24 hours
    prices = np.array([pyo.value(model.DA_prices[t]) for t in range(24)])
    model.lowest_price_time_step = np.argmin(prices)
    # replace lowest price by a high price to find the second lowest in the next step
    prices[model.lowest_price_time_step] = 9999
    model.second_lowest_price_time_step = np.argmin(prices)
    prices[model.second_lowest_price_time_step] = 9999
    model.third_lowest_price_time_step = np.argmin(prices)
    # possible curtailment events taken from inputs file (default: 0, 50 and 100 percent curtailment)

    def act_factor_init(model, t):
        if 'day-ahead variable' in model.cm_method:
            if t == model.lowest_price_time_step:
                return configs['ACM available cap']['lowest price']
            elif t == model.second_lowest_price_time_step or t == model.third_lowest_price_time_step:
                return configs['ACM available cap']['next lowest']
        # other methods and non-lowest price time steps: no activation of day-ahead limitation
        return 1

    # initialize capacity limitation factor
    model.act_factor = pyo.Param(model.time_ind, initialize=act_factor_init)

    model.congested_times = set()

    def curt_prob_init(model, t, c):
        """
        probabilities of curtailment events. We assume a balanced curtailment scenario at lowest price time
        and a descending curtailment prob. scenario at second lowest time.
        :param model:
        :param t: time index
        :param c: curtailment percentage
        :return: probability of c percentage curtailment at time t
        """
        if 'Interruptible' in model.cm_method:
            if 'naive' in model.cm_method:
                # in naive version we are always optimistic that no curtailment will occur
                return configs['Interruptible curtailment probabilities']['optimistic'][c]
            # when we're not naive, we're anticipating some curtailment at lowest price time steps:
            else:
                if t == model.lowest_price_time_step:
                    curtailment_prob = configs['Interruptible curtailment probabilities']['balanced']
                    if '90-0-10' in model.cm_method:
                        curtailment_prob = {0: 0.9, 50: 0, 100: 0.1}
                    elif '80-20-0' in model.cm_method:
                        curtailment_prob = {0: 0.80, 50: 0.20, 100: 0}
                elif t == model.second_lowest_price_time_step:
                    curtailment_prob = configs['Interruptible curtailment probabilities']['descending']
                    if '90-0-10' in model.cm_method:
                        curtailment_prob = {0: 0.90, 50: 0, 100: 0.10}
                    elif '80-20-0' in model.cm_method:
                        curtailment_prob = {0: 0.80, 50: 0.20, 100: 0}
                else:
                    curtailment_prob = configs['Interruptible curtailment probabilities']['optimistic']
                return curtailment_prob[c]
        else:
            # in non Interruptible proposals we assume there is never any curtailment, all the constraints are known
            # to energy suppliers in advance
            return configs['Interruptible curtailment probabilities']['optimistic'][c]

    model.curt_prob = pyo.Param(model.time_ind, model.curt_ind, initialize=curt_prob_init, within=pyo.Any)


    def curtailment_order_init(model, t, s):
        # return a random permutation of the ev index set as the order in which EVs are curtailed
        return np.random.permutation([i for i in model.ev_ind])
    if 'Interruptible' in model.cm_method:
        model.curtailment_order = pyo.Param(model.time_ind, model.scen_ind, initialize=curtailment_order_init,
                                            within=pyo.Any)

    return model


def add_purchased_power(ID_model, DA_model):
    """
    take purchased power _output_ from DA model as _input_ for ID model
    :param ID_model: model to be initialized
    :param DA_model: solved DA model
    :return:
    """
    purchased_power_dict = {t: pyo.value(DA_model.purchased_power[t]) for t in DA_model.time_ind}
    # purchased power solution from DA model becomes a parameter for the ID model
    ID_model.purchased_power = pyo.Param(DA_model.time_ind, initialize=purchased_power_dict)
    return ID_model

def solve_model_iteratively(input_model, state, purch_power=None):
    """
    function that iteratively removes congestion until no congestion remains
    :param current_time_model: fully initialized pyomo model to be solved
    :param activation_levels:
    :param configs:
    :param state:
    :param purch_power:
    :param ID_prices:
    :return:
    """
    # congested_times = set() if state == 'day-ahead' else known_cong_times
    known_cong_times = input_model.congested_times
    new_congestion_found = True
    iteration = 0

    while new_congestion_found:
        print("Iteration " + str(iteration))
        new_congestion_found = False

        c = 0

        model = input_model.clone()
        model.congested_times = known_cong_times
        # for t in congested_times:
        #     for s in model.scen_ind:
        #         activate_cl(model, t, s, activation_level=model.activation_levels[(t, s)])
        if state == 'day-ahead':
            scenario_indexes = range(1, model.n_scenarios)
            day_ahead_optimization(model)
        else:
            scenario_indexes = [0]
            intra_day_optimization(model)

        # check whether congestion still exists in any scenario
        for t in model.time_ind:
            for s in scenario_indexes:
                if pyo.value(model.total_load[t, s, c]) > model.trafo_size:
                    # print("overload at time {} in scenario {}".format(t, s))
                    # print("overload percentage: " + str(pyo.value(model.total_load[t, s])/model.trafo_size))
                    if not t in known_cong_times:
                        new_congestion_found = True
                        known_cong_times.add(t)
                    # else:
                    #     # TO-DO::: do something about unresolved congestion, e.g. set activation level even lower
                    #     print("Congested time already known, but congestion is not resolved!!!")
                    #     new_congestion_found = False
        if new_congestion_found:
            print("Still congested. Times: ")
            print(known_cong_times)
            iteration += 1
        else:
            print("No new congested times found.")

    return model

def run_simulation(args):
    # load configuration settings and input data
    configs = load_configs(args)
    datetime_format = configs['datetime format']
    start_date = datetime.strptime(configs['start datetime'], datetime_format)
    end_date = datetime.strptime(configs['end datetime'], datetime_format)
    simulation_dates = pd.date_range(start_date, end_date, freq='{}min'.format(configs['time step']))
    time_step = configs['time step']
    opt_horizon = timedelta(hours=configs['DA horizon'])
    n_days = int((end_date - start_date).days)
    trafo_size = configs['trafo size']
    graph_time_steps = 24
    ev_numbers = range(configs['n ev start'], configs['n ev end']+1, configs['n ev step'])
    # strategies = ['on/off', 'proportional', 'max efficiency']
    cm_methods = ['Interruptible, naive', 'Interruptible, anticipating', 'day-ahead variable sub', 'static subscription']
            #
            # 'Interruptible, 90-0-10', 'Interruptible, 80-20-0'
    output_variables = ['DA purchased power', 'power imbalance ts', 'total imbalance',
                        'positive imbalance', 'negative imbalance',
                        'DA costs EV charging', 'charging costs per ev', 'unsatisfied EV demand',
                        'customer total costs', 'available cap light users',
                        'available cap medium users', 'available cap heavy users']
    outputs_dict = {output_var:
                        {n_ev:
                             {strategy: 0 for strategy in cm_methods}
                         for n_ev in ev_numbers}
                    for output_var in output_variables
                    }
    for n_evs in ev_numbers:
        print("---------- N EVs: " + str(n_evs) + " ------------")
        for cm_method in cm_methods:
            safety_margin = configs['safety margin']
            print("cm_method: " + cm_method)
            # initialize random seed for EV draws
            np.random.seed(configs['random seed'])
            data = load_data(configs, n_evs, cm_method)

            # compute total inflexible load
            powercurves_dict = {cus.ident: cus.inflex_power for cus in data['customers'].values()}
            total_inflex_load_ts = reduce(lambda x, y: x.add(y), powercurves_dict.values())

            sorted_da_prices = data['DA prices'][start_date +
                                                 timedelta(hours=24):end_date-opt_horizon].sort_values(ascending=True)
            lowest_price_times = sorted_da_prices.iloc[:n_days].index
            outputs_dict['DA purchased power'][n_evs][cm_method] = pd.Series(0,
                    index=data['customers']['A001'].inflex_power.loc[start_date+timedelta(hours=24): end_date].index)
            outputs_dict['power imbalance ts'][n_evs][cm_method] = pd.Series(0,
                                                                            index=data['customers'][
                                                                                      'A001'].inflex_power.loc[
                                                                                  start_date + timedelta(
                                                                                      hours=24): end_date].index)
            outputs_dict['charging costs per ev'][n_evs][cm_method] = {cus.ev.ev_id: 0
                                                                      for cus in data['customers'].values()
                                                                        if cus.ev is not None
                                                                      }

            outputs_dict['available cap light users'][n_evs][cm_method] = []
            outputs_dict['available cap medium users'][n_evs][cm_method] = []
            outputs_dict['available cap heavy users'][n_evs][cm_method] = []
            # re-initialize random seed to make sure scenarios are the same for each ev number:
            np.random.seed(configs['random seed'])
            rng = np.random.default_rng(seed=configs['random seed'])
            # initialize data for models, separate for each cm_method
            base_model = initialize_model(configs, data)

            # set starting date to noon of first day of simulation
            current_date = start_date.replace(hour=12)

            # simulation ends with the last day for which we still have data for the optimization time horizon
            while current_date < end_date - opt_horizon:
                ### Preparations
                print("current date: {}".format(current_date))
                # add 12 hours because DA optimization starts only 12 hours after clearing of DA market at noon
                opt_start = current_date + timedelta(hours=12)
                opt_end = opt_start + opt_horizon
                opt_times = pd.date_range(opt_start, opt_end, freq='{}min'.format(configs['time step']))
                # only the first 24 hours of the optimization outcomes are actually used, the remaining times
                # are only taken into account to improve the optimization by looking ahead further:
                schedule_end = opt_start + timedelta(hours=23)
                schedule_times = pd.date_range(opt_start, schedule_end, freq='{}min'.format(configs['time step']))

                load_scenarios = {scenario_n: generate_scenario(total_inflex_load_ts.loc[opt_start:opt_end],
                                                          configs['std dev load'], configs['decay param'])
                      for scenario_n in range(configs['n_scenarios'] + 1)}
                # +1 is for the realized scenario in ID problem

                # initialize day-ahead and intra-day model. All activation factors are pre-computed in this call.
                current_time_base_model = add_model_time_data(base_model.clone(), configs, load_scenarios, n_evs,
                                                         current_date, opt_start, opt_end, data, cm_method)

                solved_DA_model = current_time_base_model.clone()
                # solve day-ahead model
                day_ahead_optimization(solved_DA_model)
                # solved_DA_model = solve_model_iteratively(current_time_base_model, state='day-ahead')

                ID_model = current_time_base_model.clone()

                try:
                    # get purchased power for the intra-day problem from solved day-ahead problem:
                    add_purchased_power(ID_model, solved_DA_model)
                except ValueError:
                    print('Charging infeasible.')
                    outputs_dict['infeasible'][n_evs][cm_method] = True
                    break

                if 'Interruptible' in cm_method:
                    # at lowest price, randomly select amount of curtailment in balanced way
                    ID_model.curtailed_at_lowest_price = ceil(rng.choice(a=[0, 0.5, 1], p=[0.34, 0.33, 0.33])*
                                                              ID_model.n_evs)
                    # at second and third lowest price, select amount of curtailment in descending order
                    ID_model.curtailed_at_nextl_price = ceil(rng.choice(a=[0, 0.5, 1], p=[0.6, 0.3, 0.1])*
                                                               ID_model.n_evs)
                    if '90-0-10' in cm_method:
                        ID_model.curtailed_at_lowest_price = ceil(rng.choice(a=[0, 0.5, 1], p=[0.9, 0, 0.1]) *
                                                                  ID_model.n_evs)
                        ID_model.curtailed_at_secondl_price = ceil(rng.choice(a=[0, 0.5, 1], p=[0.9, 0, 0.1]) *
                                                                   ID_model.n_evs)
                    elif '80-20-0' in cm_method:
                        ID_model.curtailed_at_lowest_price = ceil(rng.choice(a=[0, 0.5, 1], p=[0.8, 0.2, 0]) *
                                                                  ID_model.n_evs)
                        ID_model.curtailed_at_secondl_price = ceil(rng.choice(a=[0, 0.5, 1], p=[0.8, 0.2, 0]) *
                                                                   ID_model.n_evs)
                    print("curtailing " + str(ID_model.curtailed_at_lowest_price))


                intra_day_optimization(ID_model)
                
                DA_purchased_power = pd.Series(data=[solved_DA_model.purchased_power[t]()
                                                     for t in solved_DA_model.time_ind], index=opt_times)
                avg_inflex_load = pd.Series(
                    data=[pyo.value(solved_DA_model.avg_inflex_load[t]) for t in solved_DA_model.time_ind],
                    index=opt_times)

                power_for_EVs = DA_purchased_power.loc[schedule_times]

                outputs_dict['DA purchased power'][n_evs][cm_method].loc[schedule_times] = \
                    outputs_dict['DA purchased power'][n_evs][cm_method].loc[schedule_times].add(DA_purchased_power)
                # plots.stacked_bar_graph(DA_purchased_power.iloc[0:graph_time_steps], trafo_size)

                charging_cost_ts = power_for_EVs[schedule_times].multiply(data['DA prices'][schedule_times])
                charging_costs_sum = charging_cost_ts.sum()
                outputs_dict['DA costs EV charging'][n_evs][cm_method] += charging_costs_sum

                positive_imbalance_ts = pd.Series(
                    data=[ID_model.net_power_pos[t, 0, 0]() for t in range(24)], index=schedule_times)
                negative_imbalance_ts = pd.Series(
                    data=[ID_model.net_power_neg[t, 0, 0]() for t in range(24)], index=schedule_times)
                # power_imbalance = pd.Series(
                #     data=[ID_model.net_power[t, 0, 0]() for t in ID_model.time_ind], index=opt_times)
                outputs_dict['power imbalance ts'][n_evs][cm_method].loc[schedule_times] = \
                    outputs_dict['power imbalance ts'][n_evs][cm_method].loc[schedule_times].add(
                        positive_imbalance_ts).subtract(negative_imbalance_ts)
                outputs_dict['total imbalance'][n_evs][cm_method] += positive_imbalance_ts.add(
                    negative_imbalance_ts).sum()
                outputs_dict['positive imbalance'][n_evs][cm_method] += positive_imbalance_ts.sum()
                outputs_dict['negative imbalance'][n_evs][cm_method] += negative_imbalance_ts.sum()
                outputs_dict['unsatisfied EV demand'][n_evs][cm_method] += sum(
                    [pyo.value(ID_model.EVUL[0, 0, ev_i]) for ev_i in ID_model.ev_ind]
                )
                # computation for available EV capacity
                for ev_i in ID_model.ev_ind:
                    this_ev = data['customers'][ev_i].ev
                    if this_ev.daily_demand > 10:
                        user_type = 'heavy users'
                    elif this_ev.daily_demand > 5:
                        user_type = 'medium users'
                    else:
                        user_type = 'light users'
                    available_cap_type = 'available cap ' + user_type
                    arrival_time = datetime.time(this_ev.arrival_time)
                    departure_time = datetime.time(this_ev.departure_time)
                    output_array = outputs_dict[available_cap_type][n_evs][cm_method]
                    for t_ind, t in enumerate(opt_times):
                        # only count capacities when vehicle is actually home
                        if _is_time_between(arrival_time, departure_time, datetime.time(t)):
                            if 'static' in cm_method:
                                output_array.append(max(
                                    this_ev.customer.base_sub - this_ev.customer.inflex_power.loc[t], 0)
                                )
                            elif 'variable' in cm_method:
                                output_array.append(max(
                                    min(
                                    this_ev.customer.base_sub +
                                    this_ev.customer.var_sub*solved_DA_model.act_factor[t_ind] -
                                    this_ev.customer.inflex_power.loc[t],
                                        11),
                                    0
                                ))
                            elif 'Interruptible' in cm_method:
                                cap = 11
                                if t_ind == ID_model.lowest_price_time_step:
                                    if ev_i in ID_model.curtailment_order[t_ind, 0][:ID_model.curtailed_at_lowest_price]:
                                       cap = ID_model.max_cap_in_curt
                                elif t_ind == ID_model.second_lowest_price_time_step:
                                    if ev_i in ID_model.curtailment_order[t_ind, 0][:ID_model.curtailed_at_nextl_price]:
                                        cap = ID_model.max_cap_in_curt
                                output_array.append(cap)

                # plots.stacked_bar_graph(power_imbalance.iloc[0:graph_time_steps], trafo_size)
                # plots.price_plot(data['DA prices'][opt_times],
                #                 data['ID prices'][opt_times])

                realized_inflex_load = load_scenarios[0].loc[opt_start:opt_end]
                try:
                    flex_power_dict = {ev_i: pd.Series(data=[pyo.value(ID_model.flex_power[t, 0, 0, ev_i])
                                                             for t in ID_model.time_ind], index=opt_times)
                                       for ev_i in ID_model.ev_ind}
                except ValueError:
                    print('Charging infeasible on day {}'.format(current_date.day))
                    outputs_dict['infeasible'][n_evs][cm_method] = True
                    break
                total_flex_load = reduce(lambda x, y: x.add(y), flex_power_dict.values())
                # charging_cost_ts = DA_purchased_power[schedule_times].multiply(data['DA prices'][schedule_times])

                # plots.stacked_bar_graph(realized_inflex_load, trafo_size, top_data=total_flex_load)
                for ev in data['evs dict'].values():
                    ### TO ADD:::
                    # data for scatter plot for consumer costs, added network tariff + power prices
                    ev_power_series = flex_power_dict[ev.customer.ident].loc[opt_start: schedule_end]
                    # we assume that charging costs for this EV are the proportion of the demand of this EV
                    # at a given time step times the total costs for this time step
                    # charging_costs = charging_cost_ts.multiply(ev_power_series).divide(
                    #     total_network_load.loc[schedule_times]).sum()
                    # ev.charging_costs += charging_costs
                    # outputs_dict['charging costs per ev'][n_evs][cm_method][ev.ev_id] += charging_costs

                    ev.update_ev_power(ev_power_series, time_step)
                    # if ev.daily_demand > 15:
                    #     if 'variable' in cm_method:
                    #         plots.variable_cap_sub(ev.customer, schedule_times, act_factors)
                    #     if 'static' in cm_method:
                    #         plots.static_cap_sub_visual(ev.customer, schedule_times)
                    # if 'Interruptible' in cm_method:
                    #     plots.interruptible(ev.customer, schedule_times)

                current_date = current_date + timedelta(hours=24)

            # outputs_dict['customer total costs'][n_evs][cm_method] = {
            #     cus.ident: cus.capacity_costs(configs['base sub kW cost'], configs['var sub kW cost'],
            #                                   simulation_dates) +
            #                cus.network_energy_costs(configs['low charge kWh'], configs['high charge kWh'],
            #                                         simulation_dates) +
            #                cus.ev.charging_costs
            #     for cus in data['customers'].values()
            #     if cus.ev is not None}

            act_factors = [pyo.value(solved_DA_model.act_factor[t]) for t in range(24)]

            graph_time_steps = 24
            # selct one light and one heavy EV user based on their daily demand
            cus_light = [ev for ev in data['evs dict'].values() if ev.daily_demand == 4][1].customer
            cus_heavy = [ev for ev in data['evs dict'].values() if ev.daily_demand == 16][0].customer
            if 'variable' in cm_method:
                plots.variable_cap_sub(cus_light, cus_heavy, schedule_times[:graph_time_steps], act_factors)
            if 'Interruptible' in cm_method:
                plots.interruptible(cus_light, cus_heavy, schedule_times[:graph_time_steps], cm_method)
            if 'static' in cm_method:
                plots.static_cap_sub_visual(cus_light, cus_heavy, schedule_times[:graph_time_steps])

        graph_times = opt_times[:graph_time_steps]
        avg_inflex_load = pd.Series(data=[pyo.value(solved_DA_model.avg_inflex_load[t]) for t in solved_DA_model.time_ind],
                                     index=opt_times)
        plots.optimization_results(outputs_dict['DA purchased power'][n_evs], avg_inflex_load, trafo_size,
                                   schedule_times[:graph_time_steps])

    # print(outputs_dict['purchased power'][n_evs][strategy].loc[lowest_price_times])

    prefix = 'outputs'
    timestamp_format = "{:%Y%m%dT%H%M}"
    timestamp = timestamp_format.format(datetime.now())
    output_filename = f"{prefix}__{timestamp}.pickle"
    with open(output_filename, 'wb') as handle:
        pickle.dump(outputs_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

    print('Total costs for EV charging: ')
    print(outputs_dict['DA costs EV charging'])
    print('Total imbalance: ')
    print(outputs_dict['total imbalance'])
    print('Positive imbalance: ')
    print(outputs_dict['positive imbalance'])
    print('Negative imbalance: ')
    print(outputs_dict['negative imbalance'])
    print('Unsatisfied EV demand: ')
    print(outputs_dict['unsatisfied EV demand'])
    # print('Available cap for EVs: ')
    # print('light users:')
    # print(outputs_dict['available cap light users'][25]['static sub'][0::10])
    # print(outputs_dict['available cap light users'][25]['day-ahead variable sub'][0::10])
    # print(outputs_dict['available cap light users'][25]['Interruptible, naive'][0::10])
    # print('medium users')
    # print(outputs_dict['available cap medium users'][25]['static sub'][0::10])
    # print(outputs_dict['available cap medium users'][25]['day-ahead variable sub'][0::10])
    # print(outputs_dict['available cap medium users'][25]['Interruptible, naive'][0::10])
    # print('heavy users')
    # print(outputs_dict['available cap heavy users'][25]['static sub'][0::10])
    # print(outputs_dict['available cap heavy users'][25]['day-ahead variable sub'][0::10])
    # print(outputs_dict['available cap heavy users'][25]['Interruptible, naive'][0::10])
    # print('Costs by EV:')
    # print(outputs_dict['charging costs per ev'])

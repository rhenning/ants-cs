import pandas as pd
from math import floor

class Customer:
    """ Customer can have flexible devices: EVs and HVAC systems.
     inflex_power is their consumption that is not due to these flexible devices. """
    def __init__(self, ident,
                 neighborhood_id,
                 inflex_power: pd.Series,
                 base_sub = None,
                 var_sub = 0,
                 ev = None,
                 hvac = None):
        self.ident = ident
        self.neighborhood_id = neighborhood_id
        self.inflex_power = inflex_power
        self.base_sub = base_sub
        self.var_sub = var_sub
        self.ev = ev
        self.hvac = hvac
        # in case a network constraint is active at the household level, the following attribute can be set:
        self.power_limit = None

    def capacity_costs(self, base_charge, var_charge, date_range):
        n_days = float((date_range[-1] - date_range[0]).days)
        n_days = 31
        # print('capacity costs: ' + str((self.base_sub*base_charge + self.var_sub*var_charge)*n_days/365))
        return (self.base_sub*base_charge + self.var_sub*var_charge)*n_days/365

    def network_energy_costs(self, kWh_charge, kWh_high_charge, date_range):
        if self.var_sub == 0:
            power_time_series = self.inflex_power.loc[date_range]
            volumetric_charge = pd.Series(0, index=pd.to_datetime(power_time_series.index))
            for t, power in power_time_series.items():
                if power > self.base_sub:
                    volumetric_charge.loc[t] += self.base_sub*kWh_charge + (power - self.base_sub)*kWh_high_charge
                else:
                    volumetric_charge.loc[t] += power*kWh_charge
            return volumetric_charge.sum()
        else:
            # we assume that with variable charge the subscribed capacity is never exceeded.
            # a bit lazy but probably true. In order to actually check this, we would need to get
            # the final activation factor for each time out of the pyomo models, which is a hazzle.
            return (self.inflex_power.loc[date_range].sum() + self.ev.ev_power.loc[date_range].sum())*kWh_charge

class EV:
    """Electric vehicles and their associated optimization constraints
    """
    def __init__(self, ev_id, customer, neighborhood_id, arrival_time, departure_time, starting_soc_perc,
                 target_soc_perc, battery_size, max_charge_power, charging_efficiency,
                 daily_demand):
        self.ev_id = ev_id
        self.customer = customer
        self.ev_power = pd.Series(0, index=pd.to_datetime(customer.inflex_power.index))
        self.location = neighborhood_id    # neighborhood id of ev's location
        self.arrival_time = arrival_time
        self.departure_time = departure_time
        self.current_soc_perc = starting_soc_perc # state of charge, initial values given at creation
        self.current_charge = starting_soc_perc*float(battery_size)
        self.target_soc_perc = target_soc_perc
        self.target_charge = target_soc_perc*float(battery_size)
        self.battery_size = battery_size
        self.max_charge_power = max_charge_power
        self.charging_efficiency = charging_efficiency
        self.daily_demand = daily_demand
        self.charging_costs = 0

    def update_ev_power(self, ev_powercurve: pd.Series, time_step):
        """
        Writes given ev power to the household object and updates its battery charge.
        :param ev_powercurve: solution from optimization or charge on arrival scheduling.
        :return: None
        """
        update_times = ev_powercurve.index
        self.ev_power.loc[update_times] = self.ev_power.loc[update_times].add(ev_powercurve)
        self.current_charge += ev_powercurve.sum()*self.charging_efficiency*60/time_step
        # reduce charge by daily demand
        self.current_charge -= self.daily_demand
        self.current_soc_perc = self.current_charge/float(self.battery_size)
        if not 0 <= self.current_soc_perc <= 1.000001:
            raise ValueError("ev state of charge out of bounds (0, 1) for household {}, ev {}: {}"
                             .format(self.customer.ident, self.ev_id, self.current_soc_perc))


class HVAC:
    """HVAC systems """
    def __init__(self, lower_comfort_limit, upper_comfort_limit,
                 starting_temp, efficiency, thermal_loss_coef):
        self.lower_comfort = lower_comfort_limit
        self.upper_comfort = upper_comfort_limit
        self.current_temp = starting_temp # temperature initial values given at creation
        self.efficiency = efficiency
        self.thermal_loss_coef = thermal_loss_coef
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as clr
import matplotlib.dates as mdates
from itertools import chain
from statistics import fmean

from model_files.customer_classes import Customer

def stacked_bar_graph(bottom_data, transformer_limit, top_data=None, title="default"):
    time_ind = np.arange(len(bottom_data))
    plt.bar(time_ind, bottom_data, width=0.7)
    if top_data is not None:
        plt.bar(time_ind, top_data, bottom=bottom_data, color='red', width=0.7)
    plt.plot(time_ind, np.full(len(time_ind), transformer_limit), ls='--', color='red')
    plt.xlabel(title)
    plt.show()

def price_plot(day_ahead_price, intraday_prices, title="Prices"):
    time_ind = np.arange(len(day_ahead_price))
    plt.scatter(time_ind, day_ahead_price, label = 'Day-ahead', color ='tab:green')
    plt.scatter(time_ind, intraday_prices, label = 'Intra-day', color ='tab:red')
    plt.legend()
    plt.show()


def variable_cap_sub(cus_light: Customer, cus_heavy: Customer, date_range, act_factors):
    # base_sub = cus.base_sub
    # inflex_load = cus.inflex_power.loc[date_range]
    plt.rcParams.update({'font.size': 18})
    cmap = clr.get_cmap('cividis')
    flex_load_light = cus_light.ev.ev_power.loc[date_range]
    flex_load_heavy = cus_heavy.ev.ev_power.loc[date_range]
    time_ind = np.arange(len(date_range))
    time_ind_var_sub = []
    for i in range(len(date_range)):
        time_ind_var_sub += [i - 0.49, i + 0.49]
    var_sub_light = [cus_light.base_sub + cus_light.var_sub * act_factors[i//2] for i in range(2*len(date_range))]
    var_sub_heavy = [cus_heavy.base_sub + cus_heavy.var_sub * act_factors[i // 2] for i in range(2*len(date_range))]
    fig, axs = plt.subplots(1, 2, sharey='row')
    axs[0].bar(time_ind, flex_load_light,
            label='EV load', color=cmap(0.95), width=0.7)
    axs[0].plot(time_ind_var_sub, var_sub_light, ls='solid', color=cmap(0.5), label='Variable Capacity')
    axs[0].set_title('Light EV user')
    axs[1].bar(time_ind, flex_load_heavy,
            color=cmap(0.95), width=0.7)
    axs[1].plot(time_ind_var_sub, var_sub_heavy, ls='solid', color=cmap(0.5))
    axs[1].set_title('Heavy EV user')
    for ax in axs:
        ax.yaxis.grid(True)
        # ax.set_xlabel('Hour of day')
        ax.set_ylim(0, 11.4)
        ax.set_ylabel('Customer load [kW]')
        ax.set_xticks([0, 4, 8, 12, 16, 20])
        ax.set_xticklabels(["00:00", "04:00", "08:00", "12:00", "16:00", "20:00"])
        # Rotates and right-aligns the x labels so they don't crowd each other.
        for label in ax.get_xticklabels(which='major'):
            label.set(rotation=22, horizontalalignment='right', fontweight='light', fontsize='small')
    fig.legend(loc='lower left', bbox_to_anchor=(0.15, 0.7))
    # fig.suptitle('Variable day-ahead capacity limitation')
    fig.set_size_inches(12.5, 6, forward=True)
    # fig.tight_layout()
    plt.show()

def interruptible(cus_light: Customer, cus_heavy: Customer, date_range, title):
    plt.rcParams.update({'font.size': 18})
    cmap = clr.get_cmap('cividis')
    flex_load_light = cus_light.ev.ev_power.loc[date_range]
    flex_load_heavy = cus_heavy.ev.ev_power.loc[date_range]
    time_ind = np.arange(len(date_range))
    curtailed_times_light = [t for t in time_ind if flex_load_light[t] > 3.699 and flex_load_light[t] < 3.701]
    curtailed_times_heavy = [t for t in time_ind if flex_load_heavy[t] > 3.699 and flex_load_heavy[t] < 3.701]
    curtailed_index_light = []
    for t in curtailed_times_light:
        curtailed_index_light.append([t-0.5, t+0.5])
    curtailed_index_heavy = []
    for t in curtailed_times_heavy:
        curtailed_index_heavy.append([t-0.5, t+0.5])
    curtailed_cap = [3.7, 3.7]
    fig, axs = plt.subplots(1, 2, sharey='row')
    axs[0].bar(time_ind, flex_load_light,
            label='EV load', color=cmap(0.95), width=0.7)
    for i, i_list in enumerate(curtailed_index_light):
        if i == 0:
            axs[0].plot(i_list, curtailed_cap, ls='solid', color=cmap(0.5), label='Curtailed capacity')
        else:
            axs[0].plot(i_list, curtailed_cap, ls='solid', color=cmap(0.5))
    axs[0].set_title('Light EV user')
    axs[1].bar(time_ind, flex_load_heavy,
            color=cmap(0.95), width=0.7)
    for i_list in curtailed_index_heavy:
        axs[1].plot(i_list, curtailed_cap, ls='solid', color=cmap(0.5))
    axs[1].set_title('Heavy EV user')
    for ax in axs:
        ax.yaxis.grid(True)
        # ax.set_xlabel('Hour of day')
        ax.set_ylim(0, 11.4)
        ax.set_ylabel('Customer load [kW]')
        ax.set_xticks([0, 4, 8, 12, 16, 20])
        ax.set_xticklabels(["00:00", "04:00", "08:00", "12:00", "16:00", "20:00"])
        # Rotates and right-aligns the x labels so they don't crowd each other.
        for label in ax.get_xticklabels(which='major'):
            label.set(rotation=22, horizontalalignment='right', fontweight='light', fontsize='small')
    fig.legend(loc='lower left', bbox_to_anchor=(0.15, 0.7))
    # fig.suptitle(title)
    fig.set_size_inches(12.5, 6, forward=True)
    plt.show()

def static_cap_sub_visual(cusl: Customer, cush: Customer, date_range):
    within_sub_charge = 2
    above_sub_charge = 40
    # cap_sub = cus.base_sub
    plt.rcParams.update({'font.size': 18})
    cmap = clr.get_cmap('cividis')
    cap_subl = cusl.base_sub
    inflex_loadl = cusl.inflex_power.loc[date_range]
    flex_loadl = cusl.ev.ev_power.loc[date_range]
    cap_subh = cush.base_sub
    inflex_loadh = cush.inflex_power.loc[date_range]
    flex_loadh = cush.ev.ev_power.loc[date_range]
    time_ind = np.arange(len(date_range))
    load_above_cap_subl = pd.Series(0, index=pd.to_datetime(inflex_loadl.index))
    load_below_cap_subl = pd.Series(0, index=pd.to_datetime(inflex_loadl.index))
    load_above_cap_subh = pd.Series(0, index=pd.to_datetime(inflex_loadh.index))
    load_below_cap_subh = pd.Series(0, index=pd.to_datetime(inflex_loadh.index))
    for t, power in inflex_loadl.items():
        if power > cap_subl:
            load_above_cap_subl.loc[t] += power - cap_subl
            load_below_cap_subl.loc[t] += cap_subl
        else:
            load_below_cap_subl.loc[t] += power
    for t, power in inflex_loadh.items():
        if power > cap_subh:
            load_above_cap_subh.loc[t] += power - cap_subh
            load_below_cap_subh.loc[t] += cap_subh
        else:
            load_below_cap_subh.loc[t] += power

    fig, axs = plt.subplots(1, 2, sharey='row')
    axs[0].bar(time_ind, load_below_cap_subl, label='Inflexible load below sub. capacity',
            color= cmap(0.3), width=0.7)
    axs[0].bar(time_ind, load_above_cap_subl.iloc[time_ind], bottom=load_below_cap_subl,
            label='Inflexible load above sub. capacity', color='red', width=0.7)
    axs[0].bar(time_ind, flex_loadl, bottom=load_below_cap_subl.add(load_above_cap_subl),
            label='EV load', color=cmap(0.95), width=0.7)
    axs[0].plot(time_ind, np.full(len(time_ind), cap_subl), ls='--', color=cmap(0), label='Subscribed Capacity')
    axs[0].set_title('Light EV user')

    axs[1].bar(time_ind, load_below_cap_subh,
            color= cmap(0.3), width=0.7)
    axs[1].bar(time_ind, load_above_cap_subh.iloc[time_ind], bottom=load_below_cap_subh,
            color='red', width=0.7)
    axs[1].bar(time_ind, flex_loadh, bottom=load_below_cap_subh.add(load_above_cap_subh),
            color=cmap(0.95), width=0.7)
    axs[1].plot(time_ind, np.full(len(time_ind), cap_subh), ls='--', color=cmap(0))
    axs[1].set_title('Heavy EV user')
    for ax in axs:
        ax.yaxis.grid(True)

        ax.set_ylim(0, 11.4)
        ax.set_ylabel('Customer load [kW]')
        ax.set_xticks([0, 4, 8, 12, 16, 20])
        ax.set_xticklabels(["00:00", "04:00", "08:00", "12:00", "16:00", "20:00"])
        # Rotates and right-aligns the x labels so they don't crowd each other.
        for label in ax.get_xticklabels(which='major'):
            label.set(rotation=22, horizontalalignment='right', fontweight='light', fontsize='small')

    fig.legend(loc='lower left', bbox_to_anchor=(0.15, 0.58))
    # fig.tight_layout()
    # fig.suptitle('Static capacity subscription')
    fig.set_size_inches(12.5, 6, forward=True)
    plt.show()


def avg_ppower_plot(avg_ppower):
    ev_ind = [i for i in avg_ppower.keys()]
    cmap = clr.get_cmap('viridis')
    strategies = next(iter(avg_ppower.values())).keys()
    color_value = 0.0
    for strategy in strategies:
        data = [avg_ppower[n_evs][strategy] for n_evs in ev_ind
                if avg_ppower[n_evs][strategy] != 0.0]
        plt.plot(ev_ind[:len(data)], data, label=strategy, color=cmap(color_value))
        color_value += 0.5
    plt.xticks(ev_ind)
    plt.xlabel("EV number")
    plt.ylabel("Network load percentage")
    plt.legend()
    plt.show()

def overload_events(n_evs, emergency_events, overload, strategy):
    cmap = clr.get_cmap('viridis')
    try:
        colors = [cmap(1-(n_ev-min(n_evs))/(max(n_evs)-min(n_evs))) for n_ev in n_evs]
    except ZeroDivisionError:
        print('Error: Cannot plot overload events for a single ev number')
        return
    for i, color in enumerate(colors):
        plt.scatter(emergency_events[i], overload[i], c=color, label=n_evs[i])

    plt.title('Emergency events with ' + strategy.capitalize() + ' activation')
    plt.xlabel("Number of emergency events")
    plt.ylabel("Maximal overload percentage")
    plt.legend(title='N EVs', loc='upper left')
    plt.show()

def optimization_results(purchased_power_per_strategy, avg_inflex_load, transformer_limit, graph_times):
    ## plotting ###
    time_ind = np.arange(len(graph_times))
    cmap = clr.get_cmap('viridis')
    plt.rcParams.update({'font.size': 21})
    # plt.grid(visible=True, axis='y')
    # time_ind = purchased_power_per_strategy['unconstrained'].loc[:graph_time_steps]
    # fig, axs = plt.subplots(2, 2, sharex='col', sharey='row')
    fig, axs = plt.subplots(2, 2)
    plt.subplots_adjust(hspace=0.3)
    # fig.suptitle("Purchased power in different strategies", fontsize=12, y=0.98)
    strategies = list(purchased_power_per_strategy.keys())
    print([max([i for i in purchased_power_per_strategy[strategy]]) for strategy in strategies])
    max_load = max([max([i for i in purchased_power_per_strategy[strategy]]) for strategy in strategies])
    print(max_load)

    for strategy, ax in zip(strategies, axs.ravel()):
        # filter df for ticker and plot on specified axes
        # top_data = purchased_power_per_strategy[strategy].loc[graph_times].subtract(avg_inflex_load.loc[graph_times])
        # ax.bar(time_ind, avg_inflex_load.loc[graph_times], width=0.7, color=cmap(0.3), alpha=0.25,
        #        label="exp. base load")
        #
        # ax.bar(time_ind, top_data, bottom=avg_inflex_load.loc[graph_times], width = 0.7, color=cmap(0.99),
        #        label="exp. flexible")
        ax.bar(time_ind, purchased_power_per_strategy[strategy].loc[graph_times], width=0.7, color=cmap(0.99),
               label="Purchased flexible load")
        # ax.plot(time_ind, np.full(len(time_ind), transformer_limit), ls='--', color='red', label="transformer limit")
        # chart formatting
        ax.set_title(strategy.capitalize())
        ax.yaxis.grid(True)
        ax.set_xlabel("")
        ax.set(ylabel='power in kW')
        ax.set_ylim(0, max_load)
        ax.set_xticks([0, 4, 8, 12, 16, 20])
        ax.set_xticklabels(["00:00", "04:00", "08:00", "12:00", "16:00", "20:00"])
        ax.set_xlabel('Hour of day')
        # Rotates and right-aligns the x labels so they don't crowd each other.
        for label in ax.get_xticklabels(which='major'):
            label.set(rotation=22, horizontalalignment='right', fontweight='light', fontsize='small')
    for ax in axs.flat:
        ax.label_outer()

    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles, labels, loc = 'upper center', ncol = 3)

    plt.show()


# def create_dummy_data():
#     free_cap = 15
#     cap_subs = [4, 4, 2, 2, 2]
#     min_sub = 1.5
#     ev_owner = [True, True, False, False, False]
#     unconst_low = [11, 2, 1, 1, 1]
#     unconst_high = [11, 11, 8, 2, 2]
#     load_dict = {'low': {'unconstrained': unconst_low}, 'high': {'unconstrained': unconst_high}}
#     for load in ['low', 'high']:
#         if load == 'high':
#             cap_subs[2] = 4
#             ev_owner[2] = True
#         on_off_load = [cap_subs[i] if not cap_subs[i] > unc_load else unc_load
#                             for i, unc_load in enumerate(load_dict[load]['unconstrained'])]
#         load_dict[load]['on/off'] = on_off_load
#         prop_load = [free_cap/len(load_dict[load]['unconstrained'])
#                      if not free_cap/len(load_dict[load]['unconstrained']) > uload else uload
#                     for uload in load_dict[load]['unconstrained']]
#         load_dict[load]['proportional'] = prop_load
#         # pooled load allocates free space after taking into account proportional allocation:
#         free_space_per_user = (free_cap - sum(prop_load)) / len([i for i, _ in enumerate(prop_load)
#                                                                  if prop_load[i] < load_dict[load]['unconstrained'][i]])
#         pooled_load = [p_load + free_space_per_user
#                        if p_load < load_dict[load]['unconstrained'][i] else p_load
#                        for i, p_load in enumerate(prop_load)]
#         load_dict[load]['pooled + \n proportional'] = pooled_load
#     # load_dict['low']['pooled + \n proportional'] = [7, 5, 2]
#     print(load_dict)
#
#     return load_dict, free_cap

# def concept_visualization(load_dict, free_cap):
#     cmap = clr.get_cmap('viridis')
#     # time_ind = purchased_power_per_strategy['unconstrained'].loc[:graph_time_steps]
#     # fig, axs = plt.subplots(2, 2, sharex='col', sharey='row')
#     fig, axs = plt.subplots(1, 2, sharey='row')
#     plt.subplots_adjust(hspace=0.3)
#     load_types = load_dict.keys()
#     for i, load_type in enumerate(load_dict.keys()):
#         bottom = np.zeros(4)
#         n_ev = len(next(iter(load_dict[load_type].values())))
#         strategies = np.array([strat for strat in load_dict[load_type].keys()])
#         axs[i].axhline(y = free_cap, ls='--', color='red', label="free capacity")
#         axs[i].set_title(load_type.capitalize() + ' congestion event')
#         axs[i].set(ylabel='Available network capacity in kW')
#         for ev_i in range(n_ev):
#             ev_col = float(ev_i)/(n_ev-1)
#             if load_type == 'low':
#                 ev_label = "User " + str(ev_i+1) + " (w EV)" if ev_i <= 1 else "User " + str(ev_i+1)
#             else:
#                 ev_label = "User " + str(ev_i+1) + " (w EV)" if ev_i <= 2 else "User " + str(ev_i+1)
#             ev_load = np.array([load_dict[load_type][strategy][ev_i] for strategy in strategies])
#             print(strategies)
#             print(ev_load)
#             axs[i].bar(strategies, ev_load, bottom=bottom, width = 0.6, label=ev_label, color=cmap(ev_col))
#             axs[i].legend()
#             bottom += ev_load
#     for ax in axs.flat:
#         ax.label_outer()
#     plt.show()

def available_cap_distribution(outputs_dict):
    cmap = clr.get_cmap('viridis')
    indices = [1,2,3,5,6,7,9,10,11]
    tick_labels = ['light', 'medium', 'heavy', 'light', 'medium', 'heavy', 'light', 'medium', 'heavy']
    available_caps = ['available cap light users', 'available cap medium users', 'available cap heavy users']
    data = [outputs_dict[cap][25]['static sub'] for cap in available_caps] + \
           [outputs_dict[cap][25]['day-ahead variable sub'] for cap in available_caps] + \
           [outputs_dict[cap][25]['Interruptible, naive'] for cap in available_caps]
    plt.rcParams.update({'font.size': 21})
    plt.violinplot(data, positions=indices, showextrema=False, widths=0.85, points=5000, bw_method=0.1)
    plt.ylim(0,12.4)
    plt.grid(visible=True, axis='y')
    plt.ylabel('Available capacity for EV in kW')
    plt.xlabel('User type')
    plt.text(x=1, y=11.3, s='Static cap. sub.')
    plt.text(x=5, y=11.3, s='Day-ahead CLC')
    plt.text(x=9, y=11.3, s='Interruptible')
    plt.xticks(ticks=indices, labels=tick_labels, rotation=22,
               horizontalalignment='right', fontweight='light')
    plt.show()



if __name__ == '__main__':
    import pickle
    # create dummy data for plots
    # n_evs = [25, 30, 35, 40]
    # emergency_events = [1, 5, 9, 16]
    # highest_overload = [5, 16, 35, 56]
    # overload_events(n_evs, emergency_events, highest_overload, 'on/off')
    # load_dict, free_cap = create_dummy_data()
    # concept_visualization(load_dict, free_cap)
    with open('outputs__20230718T1216.pickle', 'rb') as handle:
        saved_outputs = pickle.load(handle)
    available_cap_distribution(saved_outputs)
    available_caps = ['available cap light users', 'available cap medium users', 'available cap heavy users']
    strategies = ['Interruptible, naive', 'Interruptible, anticipating', 'day-ahead variable sub', 'static sub']
    print('Avg available capacity for flexible loads: ')
    print({strategy: [fmean(saved_outputs[cap][25][strategy]) for cap in available_caps] for strategy in strategies})




